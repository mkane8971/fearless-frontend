
function createCard(title, description, pictureUrl, formattedStarts, formattedEnds, locationName) {
        return `
        <div className="col">
            <div className="shadow-lg p-3 mb-5 bg-body-tertiary rounded">
                <div className="card">
                    <img src="${pictureUrl}", className="card-img-top">
                    <div className="card-body">
                        <h5 className="card-title">${title}</h5>
                        <h6 className="card-subtitle mb-2 text-muted">${locationName}</h6>
                        <p className="card-text">${description}</p>
                    </div>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">${formattedStarts} - ${formattedEnds}</li>
                </div>
            </div>
        </div>
        `;
}

function throwError(){
    return `
    <div className="alert alert-primary" role="alert">
        Something isnt loading!
    </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        if (!response.ok) {
            const errDoc = document.querySelector('.row')
            const errMsg = errDoc.innerHTML += throwError()
            console.error(throwError())
            throw new Error(errMsg)

        // Figure out what to do when the response is bad
        } else {
            const data = await response.json();
            for (let card of data.conferences){

            }
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const title = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const locationName = details.conference.location.name;
                  const formattedStarts = new Date(details.conference.starts).toLocaleDateString();
                  const formattedEnds = new Date(details.conference.ends).toLocaleDateString();
                  const html = createCard(title, description, pictureUrl, formattedStarts, formattedEnds, locationName);
                  const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
              }

            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok) {
            //     const details = await detailResponse.json();
            //     console.log(details)

            //     const detailConference = details.conference.description;
            //     const detailTag= document.querySelector('.card-text');
            //     detailTag.innerHTML = detailConference;

            //     const imgTag = document.querySelector('.card-img-top');
            //     imgTag.src = details.conference.location.picture_url;
            // }


        }
    } catch (e) {
        const errDoc = document.querySelector('.row')
        const errMsg = errDoc.innerHTML += throwError()
        console.error(throwError())
        throw new Error(errMsg)

      // Figure out what to do if an error is raised
    }

  });
